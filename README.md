**Как запустить проект:**

*python3 manage.py migrate*

*python3 manage.py runserver*

**В папке settings два файла:**

local.py - Для локального запуска в DEBUG режиме

default.py - Для запуска на сервере

**Запуск тестов**

_cd payments/_

Models:

_python manage.py test payments.apps.bank.tests.test_models_

Forms:

_python manage.py test payments.apps.bank.tests.test_forms_

Views:

_python manage.py test payments.apps.bank.tests.test_views_
