from django import forms
from django.utils.translation import ugettext_lazy as _
from collections import OrderedDict

from .models import User


def diff_lists(list1, list2):
    lists_diff = [i for i in list1 + list2
                  if i not in list1 or i not in list2]
    lists_diff = list(OrderedDict.fromkeys(lists_diff))
    return lists_diff


class PaymentForm(forms.Form):
    user = forms.ModelChoiceField(label=_('От пользователя'), queryset=None, empty_label="Выберите пользователя")
    inn = forms.CharField(label=_('Пользователям (ИНН)'))
    amount = forms.DecimalField(label=_('Сумма'), max_digits=15, decimal_places=2)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['user'].queryset = User.objects.all().order_by('id')
        self.fields['user'].help_text = 'Выберите пользователя с счёта которого необходимо списать у.е.'
        self.fields['inn'].help_text = 'Введите ИНН пользователей через запятую, которым нужно перевести у.е.'
        self.fields['amount'].help_text = 'Введите сумму, которую необходимо перевести пользователям'

    def clean(self):
        cleaned_data = super().clean()
        user = cleaned_data.get("user", "")
        inn = cleaned_data.get("inn", "")
        amount = cleaned_data.get("amount", "")
        try:
            inns = [item.strip() for item in inn.split(',')]
            inn_exits = User.objects.filter(inn__in=inns).values('inn')
            inn_exits = [x.get('inn') for x in inn_exits]
            diff = diff_lists(inns, inn_exits)
        except Exception as err:
            print('Ошибка разбора ИНН {}'.format(err))
            self.add_error('inn', "Заполните поле правильно")
        else:
            if inn and diff:
                msg = "ИНН {} нет в базе данных".format(', '.join(diff))
                self.add_error('inn', msg)

        if amount:
            if user.account < amount:
                msg = "У пользователя: {}, недостаточно средств на счету.\nНа счету {}".format(user.username,
                                                                                               str(user.account))
                self.add_error('amount', msg)
        else:
            self.add_error('amount', "Необходимо ввести сумму")
