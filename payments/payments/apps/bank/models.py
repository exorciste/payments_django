from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from django.db import models


class User(AbstractUser):
    inn = models.CharField(_('Инн'), max_length=12, blank=False)
    account = models.DecimalField(_('Счет'), max_digits=15, decimal_places=2, default=0.0)

    class Meta:
        verbose_name = _('Пользователь')
        verbose_name_plural = _('Пользователи')

    def __str__(self):
        return '{} - {}'.format(self.username, self.account)
