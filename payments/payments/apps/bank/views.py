import random
import string

from django.contrib import messages
from django.db import transaction, IntegrityError
from django.db.models import F
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import FormView
from django.views.generic.base import View

from .models import User
from .forms import PaymentForm


class PaymentFormView(FormView):
    template_name = 'index.html'
    form_class = PaymentForm
    success_url = '/'

    def form_valid(self, form):
        user_id = form.cleaned_data.get('user', '')
        inn = form.cleaned_data.get('inn', '')
        amount = form.cleaned_data.get('amount', '')
        if user_id:
            inns = [item.strip() for item in inn.split(',')]
            try:
                with transaction.atomic():
                    User.objects.filter(id=user_id.id).update(account=F('account') - float(amount))
                    ids = User.objects.filter(inn__in=inns).values('id')
                    amount_divide = float(amount) / len(ids)
                    User.objects.filter(inn__in=inns).update(account=F('account') + amount_divide)
            except IntegrityError as err:
                print('Ошибка отправки формы {}'.format(err))
                messages.error(self.request, 'Ошибка отправки формы')
            else:
                messages.success(self.request,
                                 'Форма успешно отправлена. Вы перевели по {} у.е., на {} счет(а)'.format(
                                     amount_divide, 2, len(ids)))
        return super().form_valid(form)


def index(request):
    template_name = "index.html"
    form = PaymentForm()
    context = {
        'form': form
    }

    return render(request, template_name, context)


class CreateRandomUsers(View):
    """Создаём 10 пользователей"""

    @transaction.atomic
    def get(self, request):
        count = 10
        for i in range(count):
            username = ''.join(random.choice(string.ascii_lowercase) for _ in range(8))
            inn = ''.join(random.choice(string.digits) for _ in range(12))
            password = '12345'
            account = round(random.uniform(10, 3000), 2)
            User.objects.create_user(username=username, inn=inn, password=password, account=account)

        messages.success(request, 'Создано {} пользователей'.format(count))
        return redirect(self.request.META.get('HTTP_REFERER'))
