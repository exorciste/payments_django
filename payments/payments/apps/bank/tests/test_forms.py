from django.test import TestCase

from payments.apps.bank.forms import PaymentForm
from payments.apps.bank.models import User


class PaymentFormTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        User.objects.create_user(username='andrey', inn='123456789012', password='12345', account=1000)
        User.objects.create_user(username='sasha', inn='123456789013', password='12345', account=1000)
        User.objects.create_user(username='fedor', inn='123456789014', password='12345', account=1000)
        User.objects.create_user(username='dima', inn='123456789015', password='12345', account=1000)

    def test_payment_form_user_field(self):
        form = PaymentForm()
        self.assertTrue(form.fields['user'].label is None or form.fields['user'].label == 'От пользователя')

    def test_payment_form_inn_field(self):
        form = PaymentForm()
        self.assertTrue(form.fields['inn'].label is None or form.fields['inn'].label == 'Пользователям (ИНН)')

    def test_payment_form_amount_field(self):
        form = PaymentForm()
        self.assertTrue(form.fields['amount'].label is None or form.fields['amount'].label == 'Сумма')

    def test_payment_form_once_inn(self):
        form_data = {'user': '1', 'inn': '123456789013', 'amount': 10.0}
        form = PaymentForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_payment_form_few_inn(self):
        form_data = {'user': '1', 'inn': '123456789013, 123456789014, 123456789015', 'amount': 10.0}
        form = PaymentForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_payment_form_incorrect_inn(self):
        form_data = {'user': '1', 'inn': '123456789012, 8888, 9999, 1111', 'amount': 10.0}
        form = PaymentForm(data=form_data)
        self.assertTrue(form.errors.get('inn', False))

    def test_payment_form_more_amount(self):
        form_data = {'user': '1', 'inn': '123456789013', 'amount': 1001.0}
        form = PaymentForm(data=form_data)
        self.assertTrue(form.errors.get('amount', False))

    def test_payment_form_not_fill(self):
        form_data = {'user': '1', 'inn': '', 'amount': ''}
        form = PaymentForm(data=form_data)
        self.assertTrue(form.errors.get('inn') and form.errors.get('amount'))
