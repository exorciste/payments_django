from django.test import TestCase, Client
from django.urls import reverse_lazy

from payments.apps.bank.forms import PaymentForm
from payments.apps.bank.models import User


class IndexViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        User.objects.create_user(username='andrey', inn='123456789012', password='12345', account=1000)
        User.objects.create_user(username='sasha', inn='123456789013', password='12345', account=1000)
        User.objects.create_user(username='fedor', inn='123456789014', password='12345', account=1000)
        User.objects.create_user(username='dima', inn='123456789015', password='12345', account=1000)

        for i in range(3):
            username = '{}-{}'.format('user', i)
            User.objects.create_user(username=username, inn='9876543210', password='12345', account=1000)

    def setUp(self):
        self.client = Client()

    def test_view_status_url(self):
        resp = self.client.get('')
        self.assertEqual(resp.status_code, 200)

    def test_view_status_reverse(self):
        resp = self.client.get(reverse_lazy('index'))
        self.assertEqual(resp.status_code, 200)

    def test_view_template_content(self):
        resp = self.client.get(reverse_lazy('index'))
        self.assertEqual(resp.status_code, 200)
        self.assertTrue('form' in resp.context)

    def test_view_money_transfer_one_inn(self):
        form_data = {'user': '1', 'inn': '123456789013', 'amount': 10.0}
        form = PaymentForm(data=form_data)
        resp = self.client.post(reverse_lazy('index'), data=form.data)
        user_from = User.objects.get(id=1)
        user_to = User.objects.get(inn='123456789013')
        self.assertEqual(user_from.account, 990.00)
        self.assertEqual(user_to.account, 1010.00)
        self.assertEqual(resp.status_code, 302)

    def test_view_money_transfer_few_different_inn(self):
        inns_list = ['123456789013', '123456789014', '123456789015']
        form_data = {'user': '1', 'inn': ','.join(inns_list), 'amount': 60.0}
        form = PaymentForm(data=form_data)
        resp = self.client.post(reverse_lazy('index'), data=form.data)
        user_from = User.objects.get(id=1)
        user_to = User.objects.filter(inn__in=inns_list)
        self.assertEqual(user_from.account, 940.00)
        for user in user_to:
            self.assertEqual(user.account, 1020.00)
        self.assertEqual(resp.status_code, 302)

    def test_view_money_transfer_one_inn_double(self):
        inns_list = ['9876543210']
        form_data = {'user': '1', 'inn': ','.join(inns_list), 'amount': 90.0}
        form = PaymentForm(data=form_data)
        resp = self.client.post(reverse_lazy('index'), data=form.data)
        user_from = User.objects.get(id=1)
        user_to = User.objects.filter(inn__in=inns_list)
        self.assertEqual(user_from.account, 910.00)
        for user in user_to:
            self.assertEqual(user.account, 1030.00)
        self.assertEqual(resp.status_code, 302)

    def test_view_money_transfer_few_same_inn(self):
        inns_list = ['9876543210', '9876543210']
        form_data = {'user': '1', 'inn': ','.join(inns_list), 'amount': 90.0}
        form = PaymentForm(data=form_data)
        resp = self.client.post(reverse_lazy('index'), data=form.data)
        user_from = User.objects.get(id=1)
        user_to = User.objects.filter(inn__in=inns_list)
        self.assertEqual(user_from.account, 910.00)
        for user in user_to:
            self.assertEqual(user.account, 1030.00)
        self.assertEqual(resp.status_code, 302)
