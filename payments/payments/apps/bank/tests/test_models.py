from django.test import TestCase

from payments.apps.bank.models import User


class UserModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        User.objects.create_user(username='andrey', inn='123456789012', password='12345', account=1000)
        User.objects.create_user(username='sasha', inn='123456789013', password='12345', account=1000)
        User.objects.create_user(username='fedor', inn='123456789014', password='12345', account=1000)
        User.objects.create_user(username='dima', inn='123456789015', password='12345', account=1000)

    def test_inn_label(self):
        user = User.objects.get(id=1)
        field_label = user._meta.get_field('inn').verbose_name
        self.assertEquals(field_label, 'Инн')

    def test_account_label(self):
        user = User.objects.get(id=1)
        field_label = user._meta.get_field('account').verbose_name
        self.assertEquals(field_label, 'Счет')

    def test_inn_max_length(self):
        author = User.objects.get(id=1)
        max_length = author._meta.get_field('inn').max_length
        self.assertEquals(max_length, 12)

    def test_account_max_digits(self):
        author = User.objects.get(id=1)
        max_digits = author._meta.get_field('account').max_digits
        self.assertEquals(max_digits, 15)
