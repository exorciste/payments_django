from django.apps import AppConfig


class BankConfig(AppConfig):
    name = 'payments.apps.bank'
