from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('formsubmit/', views.PaymentFormView.as_view(), name='formsubmit'),
    path('add-users/', views.CreateRandomUsers.as_view(), name='create_users'),
]
