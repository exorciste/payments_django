from .default import *

# Set local_settings if exits
try:
    from .local import *
except ImportError:
    pass
